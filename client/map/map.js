import { HandRequests } from '../../imports/collections/hand-requests';

Template.map.onRendered(function () {
  GoogleMaps.load({ key: Meteor.settings.public.googleMapsApiKey });
});

Template.map.onCreated(function () {
  Meteor.subscribe('handrequests');

  GoogleMaps.ready('googleMap', function (googleMap) {
    const handRequests = HandRequests.find().fetch();
    if (handRequests.length) {
      const testMarker = new google.maps.Marker({
        position: new google.maps.LatLng(handRequests[0].latitude, handRequests[0].longitude),
        map: googleMap.instance
      });
    }

    googleMap.instance.addListener('click', function (event) {
      openAddHandRequestDialog(googleMap.instance, event.latLng);
    });
  });
});

Template.map.helpers({
  googleMapOptions: function () {
    if (GoogleMaps.loaded()) {
      return {
        center: new google.maps.LatLng(49.770528, 15.494583),
        zoom: 6
      }
    }
  }
});

function openAddHandRequestDialog(googleMap, latLng) {
  const addHandRequestDialogHtml = Blaze.toHTMLWithData(Template.handRequestDialog, { latLng: latLng });
  const addHandRequestDialog = new google.maps.InfoWindow({
    position: latLng,
    content: addHandRequestDialogHtml
  });
  addHandRequestDialog.open(googleMap)
}
