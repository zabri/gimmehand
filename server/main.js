import { Meteor } from 'meteor/meteor';
import { HandRequests } from '../imports/collections/hand-requests';

Meteor.startup(() => {
  Meteor.publish('handrequests', function () {
    return HandRequests.find();
  });
});
