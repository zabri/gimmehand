let handRequests = new Mongo.Collection('handrequests');

handRequests.schema = new SimpleSchema({
  latitude: { type: Number },
  longitude: { type: Number }
});

export const HandRequests = handRequests;
